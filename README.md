# Ambisonic upmixing in non-realtime using SATIE spatialization engine in SuperCollider
In this repo, you'll find a WIP of the audio conversion pipeline developed by Vincent Cusson during his internship at SAT's Metalab.

The audio processing is done in Supercollider and python is used to facilitate the batch export.

## Supercollider files
###### HOA_experiments.scd
Experimentation with the HOA library; RT and NRT, with and without SATIE, etc.
###### NRT_experiments.scd
Experimentation with the NRT functionality of SuperCollider; With and without SATIE, with buffer and diskin, etc.
###### NRT_HOA_31Chan.scd
Working example of our 31 channels dome configuration upmixed in ambisonic in NRT with SATIE
###### NRT_HOA_5point1.scd
Working examples of a 5.1 channels upmixed in ambisonic in NRT with SATIE

## Python files

###### satie-convert.py
Python command line utility script to convert stereo files into mono 

Needed because the SuperCollider script takes mono files only

Two positional arguments; inputDir and outputDir

Make use of stereoToMono.py

Requirements:

* SoX v14.4.2
	https://sox.sourceforge.net
* pysox v1.3.7
	https://github.com/rabitt/pysox

###### batchExport.py
Python command line utility script to automate the ambisonic conversion pipeline.

Make use of userInput.py
