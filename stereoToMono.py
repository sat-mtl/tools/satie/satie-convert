'''
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import os
import sox
#from sox import VALID_FORMATS

class stereoToMono():
    '''Stereo to mono conversion'''
    def __init__(self):
        pass

    def conversion(inDir, outDir):    
        # If output_dir does not exists, creates it
        if not os.path.exists(outDir):
            os.makedirs(outDir)

        for file in os.listdir(inDir):
            if not file.startswith('.') and os.path.splitext(file)[-1] in sox.file_info.VALID_FORMATS: 
                if sox.file_info.channels(os.path.join(inDir,file)) != 1:    
                    print(file, 'is not mono. Converting...')
                    sox.Transformer().remix().build(os.path.join(inDir,file), os.path.join(outDir,file))
